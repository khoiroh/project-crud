package com.crud.react.controller;

import com.crud.react.dto.ProduckDTO;
import com.crud.react.modal.LoginUser;
import com.crud.react.modal.Produck;
import com.crud.react.repository.ProduckRepository;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProduckService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.print.attribute.standard.Media;
import java.util.List;

@RestController
@RequestMapping("/produck")
public class ProduckController {
    @Autowired
    ProduckRepository produckRepository;
    @Autowired
    ProduckService produckService;
    @Autowired
    ModelMapper modelMapper;

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Produck> addProduck(ProduckDTO produckDTO, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(produckService.addProduck(modelMapper.map(produckDTO, Produck.class), multipartFile));
    }
    @GetMapping("/{id}")
    public CommonResponse<Produck> getProdukById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(produckService.getProduckById(id)) ;
    }

    @PutMapping(consumes = "multipart/form-data", path = "/{id}")
    public CommonResponse<Produck> editProduckById(@PathVariable("id") Long id,Produck produck,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(produckService.editProduckById(id, produck.getNama(), produck.getDeskripsi(),produck.getImg(),produck.getHarga(), multipartFile)) ;
    }

    @GetMapping("/all-produck")
    public CommonResponse<Page<Produck>> getAllProduck(@RequestParam(required = false) int page, @RequestParam(required = false) String nama) {
        return ResponseHelper.ok(produckService.getAllProduck(page, nama==null?"":nama)) ;
    }

    @DeleteMapping("/{id}")
    public void deleteProduckById(@PathVariable("id") Long id) {produckService.deleteProduckById(id);}

}
