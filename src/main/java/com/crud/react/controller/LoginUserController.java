package com.crud.react.controller;

import com.crud.react.dto.LoginDTO;
import com.crud.react.dto.UserDTO;
import com.crud.react.modal.LoginUser;
import com.crud.react.repository.LoginUserRepository;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.LoginUserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/loginUser")
public class LoginUserController {
    @Autowired
    LoginUserRepository loginUserRepository;
    @Autowired
    LoginUserService loginUserService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDTO loginDTO) {
        return ResponseHelper.ok(loginUserService.login(loginDTO));
    }

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<LoginUser> addLoginUser(UserDTO userDTO, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(loginUserService.addLoginUser(modelMapper.map(userDTO, LoginUser.class), multipartFile));
    }

    @GetMapping("/{id}")
    public CommonResponse<LoginUser> getLoginUserById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(loginUserService.getLoginUserById(id));
    }

    @PutMapping(consumes = "multipart/form-data", path = "/{id}")
    public CommonResponse<LoginUser> editLoginUser(@PathVariable("id") Long id, UserDTO userDto,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(loginUserService.editLoginUser(id, modelMapper.map(userDto, LoginUser.class), multipartFile));
    }

    @GetMapping("/all")
    public CommonResponse<List<LoginUser>> getAllLoginUser() {
        return ResponseHelper.ok(loginUserService.getAllLoginUser());
    }

    @DeleteMapping("/{id}")
    public void deleteLoginUserById(@PathVariable("id") Long id) {
        loginUserService.deleteLoginUserById(id);
    }
}
