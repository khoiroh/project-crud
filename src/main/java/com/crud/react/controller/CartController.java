package com.crud.react.controller;

import com.crud.react.dto.CartDTO;
import com.crud.react.modal.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

  import java.util.Map;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    @PostMapping
    public CommonResponse<Cart> create(@RequestBody CartDTO cart) {
        return ResponseHelper.ok(cartService.create(cart));
    }
    @GetMapping
    public CommonResponse<Page<Cart>> findAll(@RequestParam int page, @RequestParam Long userId) {
        return ResponseHelper.ok( cartService.findAll(page, userId));
    }
    @PutMapping("/{id}")
    public CommonResponse<Cart> update(@PathVariable("id") Long id, @RequestBody CartDTO cart){
        return ResponseHelper.ok(cartService.update(id, cart));
    }
    @DeleteMapping("/{id}")
    public CommonResponse< Map<String, Object>> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(cartService.delete(id));
    }
    @DeleteMapping("/checkout")
    public CommonResponse<Map<String, Boolean>> checkout(@RequestParam Long userId) {
        return ResponseHelper.ok(cartService.checkout(userId));
    }
}
