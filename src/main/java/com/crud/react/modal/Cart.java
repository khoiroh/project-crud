package com.crud.react.modal;

import javax.persistence.*;

@Entity
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quantity")
    private int quantity;
    @Column(name = "total_price")
    private Integer totalPrice;
    @ManyToOne
    @JoinColumn(name = "produck_id")
    private Produck produckId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private LoginUser userId;

    public Cart() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Produck getProduckId() {
        return produckId;
    }

    public void setProduckId(Produck produckId) {
        this.produckId = produckId;
    }

    public LoginUser getUserId() {
        return userId;
    }

    public void setUserId(LoginUser userId) {
        this.userId = userId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", totalPrice=" + totalPrice +
                ", produckId=" + produckId +
                ", userId=" + userId +
                '}';
    }
}
