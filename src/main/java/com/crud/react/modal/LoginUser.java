package com.crud.react.modal;

import com.crud.react.enumeted.LoginUser.UserType;

import javax.persistence.*;

@Entity
@Table(name = "loginUser")
public class LoginUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "foto_profil")
    private String fotoProfil;
    @Column(name = "no_telpon")
    private Integer noTelpon;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserType role;

//    @Enumerated(EnumType.STRING)
//    @Column(name = "user_type")
//    private UserType userType;

    public LoginUser() {
    }

    public LoginUser(String email, String password, String nama, String alamat, String fotoProfil, Integer noTelpon) {
        this.email = email;
        this.password = password;
        this.nama = nama;
        this.alamat = alamat;
        this.fotoProfil = fotoProfil;
        this.noTelpon = noTelpon;
    }

    //    public UserType getUserType() {
//        return userType;
//    }
//
//    public void setUserType(UserType userType) {
//        this.userType = userType;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Integer getNoTelpon() {
        return noTelpon;
    }

    public void setNoTelpon(Integer noTelpon) {
        this.noTelpon = noTelpon;
    }

    public String getFotoProfil() {
        return fotoProfil;
    }

    public void setFotoProfil(String fotoProfil) {
        this.fotoProfil = fotoProfil;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
