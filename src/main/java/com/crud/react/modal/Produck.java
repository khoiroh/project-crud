package com.crud.react.modal;

import javax.persistence.*;

@Entity
@Table(name = "produck")
public class Produck {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Lob
    @Column(name = "deskripsi")
    private String deskripsi;

    @Lob
    @Column(name = "img")
    private String img;

    @Column(name = "harga")
    private Integer harga;

    public Produck() {
    }

    public Produck(String img, String nama, String deskripsi, Integer harga) {
        this.img = img;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Produck{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", img='" + img + '\'' +
                ", harga=" + harga +
                '}';
    }
}
