package com.crud.react.repository;

import com.crud.react.modal.Cart;
import com.crud.react.modal.Produck;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    @Query(value = "select * from cart where user_id = :user_id", nativeQuery = true)
    Page<Cart> findAllByLoginUser(Long user_id, Pageable pageable);
    @Query(value = "select * from cart where user_id = :user_id", nativeQuery = true)
    List<Cart> checkout(Long user_id);
}
    