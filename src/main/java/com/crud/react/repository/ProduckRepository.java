package com.crud.react.repository;

import com.crud.react.modal.Produck;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProduckRepository extends JpaRepository<Produck, Long> {
    @Query(value = "select * from produck where nama LIKE CONCAT('%',:nama, '%')", nativeQuery = true)
    Page<Produck> findByNama(String nama, Pageable pageable);
}
