package com.crud.react.jwt;

import com.crud.react.exception.InternalEror;
import com.crud.react.exception.NotFoundException;
import com.crud.react.modal.LoginUser;
import com.crud.react.modal.TemporaryToken;
import com.crud.react.repository.LoginUserRepository;
import com.crud.react.repository.TemporaryTokenRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    @Autowired
    private LoginUserRepository loginUserRepository;
    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;
    private  static  String seccetKey = "belajar spring";

    private static Integer expired = 900000;

//    public String generateToken(UserDetails userDetails) {
//        return Jwts.builder()
//                .setSubject(userDetails.getUsername())
//                .setIssuedAt(new Date())
//                .setExpiration(new Date(new Date().getTime() + expired))
//                .signWith(SignatureAlgorithm.HS512, seccetKey)
//                .compact();
//    }

    public String generateToken(UserDetails userDetails){
        String token = UUID.randomUUID().toString().replace("-", "");
        LoginUser loginUser = loginUserRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User notFound generate token"));
        var chekingToken = temporaryTokenRepository.findByUserId(loginUser.getId());
        if (chekingToken.isPresent()) temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(loginUser.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalEror("Token error parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token Kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }

//    public String getSubject(String token) {
//        return Jwts.parser().setSigningKey(seccetKey).parseClaimsJws(token).getBody().getSubject();
//    }
//
//    public boolean checkingTokenJwt(String token) {
//        try {
//            Jwts.parser().setSigningKey(seccetKey).parseClaimsJws(token);
//            return true;
//        } catch (SignatureException e) {
//            System.out.println("Invalid JWT signature -> Massange: {} ");
//        } catch (MalformedJwtException e) {
//            System.out.println("Invalid JWT token -> Massange: {} ");
//        } catch (ExpiredJwtException e) {
//            System.out.println("Expired JWT token -> Massange: {} ");
//        } catch (UnsupportedJwtException e) {
//            System.out.println("Unsupportted JWT token -> Massange: {} ");
//        } catch (IllegalArgumentException e) {
//            System.out.println("JWT clains string is empty -> Massange: {} ");
//        }
//        return false;
//    }
}
