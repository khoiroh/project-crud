package com.crud.react.dto;

public class CartDTO {
    private Long produckId;
    private Long userId;
    private int quantity;
    public Long getProduckId() {
        return produckId;
    }

    public void setProduckId(Long produckId) {
        this.produckId = produckId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
