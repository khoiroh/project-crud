package com.crud.react.service;

import com.crud.react.dto.CartDTO;
import com.crud.react.modal.Cart;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface CartService {
    Cart create(CartDTO cart);
    Page<Cart> findAll(int page, Long userId);
    Cart update(Long id, CartDTO cart);
    Map<String, Object> delete(Long id);
    Map<String, Boolean> checkout (Long userId);
}
