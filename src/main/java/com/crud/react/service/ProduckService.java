package com.crud.react.service;

import com.crud.react.modal.Produck;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProduckService {
    Produck addProduck (Produck produck, MultipartFile multipartFile);

    Page<Produck> getAllProduck(int page, String nama);

    Produck getProduckById(Long id);

    Produck editProduckById(Long id, String nama, String deskripsi,String img, Integer harga,MultipartFile multipartFile);

    void deleteProduckById(Long id);
}
