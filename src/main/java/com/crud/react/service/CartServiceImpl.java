package com.crud.react.service;

import com.crud.react.dto.CartDTO;
import com.crud.react.exception.NotFoundException;
import com.crud.react.modal.Cart;
import com.crud.react.modal.Produck;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.LoginUserRepository;
import com.crud.react.repository.ProduckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private LoginUserRepository loginUserRepository;
    @Autowired
    private ProduckRepository produckRepository;
    @Autowired
    private CartRepository cartRepository;

    @Override
    public Cart create(CartDTO cart) {
        Produck produck = produckRepository.findById(cart.getProduckId()).orElseThrow(() -> new NotFoundException("produck id tidak ditemukan"));
        Cart create = new Cart();
        create.setQuantity(cart.getQuantity());
        create.setTotalPrice(produck.getHarga() * cart.getQuantity());
        create.setUserId(loginUserRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
        create.setProduckId(produck);
        return cartRepository.save(create);
    }

    @Override
    public Page<Cart> findAll(int page, Long userId) {
        Pageable pageable = PageRequest.of(page, 5);

//        List<Cart> cartList = cartRepository.findById();
//        cartRepository.deleteAll(cartList);

        return cartRepository.findAllByLoginUser(userId, pageable);
    }

    @Override
    public Cart update(Long id, CartDTO cart) {
        Cart data = cartRepository.findById(id).orElseThrow(() -> new NotFoundException("id not found"));
        data.setQuantity(cart.getQuantity());
        data.setUserId(loginUserRepository.findById(cart.getUserId()).orElseThrow(() -> new NotFoundException("user id tidak ditemukan")));
        data.setProduckId(produckRepository.findById(cart.getProduckId()).orElseThrow(() -> new NotFoundException("produck id tidak ditemukan")));
        return cartRepository.save(data);
    }

    @Override
    public Map<String, Object> delete(Long id) {
        cartRepository.deleteById(id);
        Map<String, Object> obj = new HashMap<>();
        obj.put("DELETE", true);
        return obj;
    }

    @Override
    public Map<String, Boolean> checkout(Long userId) {
        List<Cart> cartList = cartRepository.checkout(userId);
        cartRepository.deleteAll(cartList);
        Map<String, Boolean> obj = new HashMap<>();
        obj.put("delete", Boolean.TRUE);
        return null;

    }
}

