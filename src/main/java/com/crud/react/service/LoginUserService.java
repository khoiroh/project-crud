package com.crud.react.service;

import com.crud.react.dto.LoginDTO;
import com.crud.react.dto.UserDTO;
import com.crud.react.modal.LoginUser;
import com.crud.react.modal.Produck;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface LoginUserService {
    Map<String, Object> login(LoginDTO loginDTO);

    LoginUser addLoginUser(LoginUser loginUser, MultipartFile multipartFile);

    LoginUser getLoginUserById(Long id);

    List<LoginUser> getAllLoginUser();

    LoginUser editLoginUser(Long id, LoginUser loginUser, MultipartFile multipartFile);

    void deleteLoginUserById(Long id);
}
