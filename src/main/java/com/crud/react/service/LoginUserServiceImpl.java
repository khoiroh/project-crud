package com.crud.react.service;

import com.crud.react.dto.LoginDTO;
import com.crud.react.dto.UserDTO;
import com.crud.react.enumeted.LoginUser.UserType;
import com.crud.react.exception.InternalEror;
import com.crud.react.exception.NotFoundException;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.modal.LoginUser;
import com.crud.react.repository.LoginUserRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LoginUserServiceImpl implements LoginUserService {
    @Autowired
    LoginUserRepository loginUserRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    LoginUserService loginUserService;
    @Autowired
    UserDetailServiceImpl userDetailService;
    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalEror("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDTO loginDTO) {
        String token = authories(loginDTO.getEmail(), loginDTO.getPassword());
        LoginUser loginUser = loginUserRepository.findByEmail(loginDTO.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", loginUser);
        return response;
    }

    private static final String DONWLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/test-crud-6f7f2.appspot.com/o/%s?alt=media";
    @Override
    public LoginUser addLoginUser(LoginUser loginUser, MultipartFile multipartFile) {
        String fotoProfil = imageConverter(multipartFile);
        LoginUser loginUser1 = new LoginUser(loginUser.getEmail(), loginUser.getPassword(), loginUser.getNama(), loginUser.getAlamat(), fotoProfil, loginUser.getNoTelpon());
        String email = loginUser.getEmail();
        loginUser.setPassword(passwordEncoder.encode(loginUser.getPassword()));
        if (loginUser.getRole().name().equals("ADMIN"))
            loginUser.setRole(UserType.ADMIN);
        else loginUser.setRole(UserType.USER);
        var validasi = loginUserRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalEror("Maaf Email sudah digunakan");
        }
        loginUser.getNama();
        loginUser.getAlamat();
        loginUser.getNoTelpon();
        loginUser.setFotoProfil(fotoProfil);
        return loginUserRepository.save(loginUser);
    }

    private String imageConverter(MultipartFile multipartFile){
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = converToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalEror("Error upload file");
        }
    }

    private File converToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException{
        BlobId blobId = BlobId.of("test-crud-6f7f2.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/ServiceAccount.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DONWLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
    private String getExtenions(String fileName){
        return fileName.split("\\.")[0];
    }

    @Override
    public LoginUser getLoginUserById(Long id) {
        return loginUserRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public List<LoginUser> getAllLoginUser() {
        return loginUserRepository.findAll();
    }

    @Transactional
    @Override
    public LoginUser editLoginUser(Long id, LoginUser loginUser, MultipartFile multipartFile) {
        LoginUser update = loginUserRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        String url = imageConverter(multipartFile);
        loginUser.setPassword(passwordEncoder.encode(loginUser.getPassword()));
        var validasi = loginUserRepository.findByEmail(loginUser.getEmail());
        if (validasi.isPresent()) {
            throw new InternalEror("Maaf email sudah ada");
        }
        update.setEmail(loginUser.getEmail());
        update.setPassword(loginUser.getPassword());
        update.setNama(loginUser.getNama());
        update.setAlamat(loginUser.getAlamat());
        update.setNoTelpon(loginUser.getNoTelpon());
        update.setFotoProfil(url);
        return update;
    }

    @Override
    public void deleteLoginUserById(Long id) {
        loginUserRepository.deleteById(id);
    }

}

