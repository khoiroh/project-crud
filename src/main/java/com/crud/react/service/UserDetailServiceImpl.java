package com.crud.react.service;

import com.crud.react.modal.LoginUser;
import com.crud.react.modal.UserPrinciple;
import com.crud.react.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private LoginUserRepository loginUserRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = loginUserRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("username not found"));
        return UserPrinciple.bulid(loginUser);
    }
}
