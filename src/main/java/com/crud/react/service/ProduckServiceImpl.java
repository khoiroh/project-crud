package com.crud.react.service;

import com.crud.react.exception.EmailCondition;
import com.crud.react.exception.InternalEror;
import com.crud.react.exception.NotFoundException;
import com.crud.react.modal.LoginUser;
import com.crud.react.modal.Produck;
import com.crud.react.repository.ProduckRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class ProduckServiceImpl implements ProduckService{
    @Autowired
    ProduckRepository produckRepository;

    private static final String DONWLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/test-crud-6f7f2.appspot.com/o/%s?alt=media";

    @Override
    public Produck addProduck(Produck produck, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);
        Produck produck1 = new Produck(url, produck.getNama(),produck.getDeskripsi(), produck.getHarga());
        return produckRepository.save(produck1);
    }

    private String imageConverter(MultipartFile multipartFile){
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = converToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalEror("Error upload file");
        }
    }

    private File converToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException{
        BlobId blobId = BlobId.of("test-crud-6f7f2.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/ServiceAccount.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DONWLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
    private String getExtenions(String fileName){
        return fileName.split("\\.")[0];
    }

    @Override
    public Page<Produck> getAllProduck(int page, String nama) {
        Pageable pages = PageRequest.of(page, 5);
        return produckRepository.findByNama(nama, pages);
    }

    @Override
    public Produck getProduckById(Long id) {

        return produckRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Produck editProduckById(Long id, String nama, String deskripsi,String img, Integer harga, MultipartFile multipartFile) {
        String url = imageConverter(multipartFile);

        Produck produck = produckRepository.findById(id).get();
        produck.setNama(nama);
        produck.setDeskripsi(deskripsi);
        produck.setHarga(harga);
        produck.setImg(url);
        return produckRepository.save(produck);    }

    @Override
    public void deleteProduckById(Long id) {
        produckRepository.deleteById(id);
    }

    }
